let db = require('../../../repositories');

module.exports.profile = (req, res) =>
    db.users.findByToken(req.headers.authorization.split(" ")[1])
        .then((user) => db.users.findById(user.id))
        .then((user) => {
            res.json(user);
        })
        .catch((err) => {
            if (!err.status)
                err.status = 500;
            res.status(err.status);
            res.json({message: err.message});
        });

module.exports.userRole = (req, res) =>
    db.users.findByToken(req.headers.authorization.split(" ")[1])
        .then((user) => db.roles.findByUserId(user))
        .then((roles) => {
            res.json(roles);
        })
        .catch((err) => {
            if (!err.status)
                err.status = 500;
            res.status(err.status);
            res.json({message: err.message});
        });

module.exports.createUser = (req, res) =>
    db.passwords.createPassword(req.body.password)
        .then((passwordId) => db.users.createUser(req.body, passwordId))
        .then((userId) => db.users.attachRoleToUser(userId, req.body.roleType))
        .then((userId) => db.userDetails.createUserDetails(userId, req))
        .then((user) => {
            res.status(200).json(user)
        }).catch((err => {
        res.status(500).json(err);
    }));


module.exports.updateUserDetailsByUserId = (req, res, next) =>
    db.users.updateUserByUserId(req)
        .then(() => db.users.updateUserRoleByUserId(req))
        .then(() => db.userDetails.updateUserDetailsByUserId(req))
        .then(() => db.saleUnit.updateAttachedSaleUnitToUser(req.body.userId, req.body['salesUnitId']))
        .then((user) => res.send({user}))
        .catch((err) => next(err));


module.exports.getAllUsers = (req, res, next) =>
    db.users.getAllUsers()
        .then((users) => res.send({users}))
        .catch((err) => next(err));





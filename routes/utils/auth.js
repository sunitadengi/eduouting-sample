let passport = require('passport');

module.exports.ensure = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        next();
    },
];


module.exports.ensureSecurity = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SECURITY') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureExpenseAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('EXPENSES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureExpenseAdminInternalAudit = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('EXPENSES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1) || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProduction = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];
module.exports.ensureQualityCheckProductionVmsAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('VMS') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProductionAdminInternalAudit = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProductionAdminInternalAuditAccount = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('ACCOUNTS') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProductionAndSecurity = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('SECURITY') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSales = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureLogistics = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('LOGISTICS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureVMS = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('VMS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureVMSAdminInternalAudit = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('VMS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];
module.exports.ensureVMSAndProduction = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('VMS') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];
module.exports.ensureVMSProductionAdminInternalAudit = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('VMS') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureBD = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('BD') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureDriverOrAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('DRIVER') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureDriver = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('DRIVER') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureAdminSalesSalesPurchaser = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('SALESPURCHASER') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];
module.exports.ensureAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureAdminAndLogistics = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('LOGISTICS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('VMS') !== -1 || req.user.roles.indexOf('DRIVER') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProductionAndSales = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProduction = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureInternalAudit = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureAccount = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('ACCOUNTS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureAccountOrInternalAuditOrVmsOrProduction = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('ACCOUNTS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('VMS') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureAccountOrInternalAuditOrBd = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('ACCOUNTS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('BD') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureAccountOrInternalAuditOrSales = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('ACCOUNTS') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('SALES') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureAll = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('VMS') !== -1 || req.user.roles.indexOf('ACCOUNTS') !== -1) || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('LOGISTICS') !== -1 || req.user.roles.indexOf('BD') !== -1 || req.user.roles.indexOf('SECURITY') !== -1 || req.user.roles.indexOf('EXPENSES') !== -1 || req.user.roles.indexOf('DRIVER') !== -1 || req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('DISPLAYPRODUCTSSUMMARY') !== -1 || req.user.roles.indexOf('COUNTERSALES') !== -1 || req.user.roles.indexOf('PRODUCTORGANISER') !== -1 || req.user.roles.indexOf('SALESPURCHASER') !== -1) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProductionAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureCustomer = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('CUSTOMER') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureProductionAndSalesAndInternalAudit = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('COUNTERSALES') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureProductionDisplayProductSummaryAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('DISPLAYPRODUCTSSUMMARY') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureProductionAdminAccountProductOrganiser = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('ACCOUNTS') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('PRODUCTORGANISER') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];
sendError = function (req, res) {
    res.status(401).json("You are not allowed to access this resource");
};

module.exports.ensureProductionProductOrganiser = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1) || req.user.roles.indexOf('PRODUCTORGANISER') !== -1) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureProductOrganiser = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1) || req.user.roles.indexOf('PRODUCTORGANISER') !== -1) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureSalesCounterAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('COUNTERSALES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSalesCounterAdminSales = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('COUNTERSALES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSalesCounterAdminSalesProduction = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('COUNTERSALES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('SALES') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSalesCounterAdminProduction = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('COUNTERSALES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSalesCounterAdminProductionSales = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('COUNTERSALES') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('SALES') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureProductOrganiserSalesPurchaser = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1) || req.user.roles.indexOf('PRODUCTORGANISER') !== -1 || req.user.roles.indexOf('SALESPURCHASER') !== -1) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSalesPurchaserAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALESPURCHASER') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('INTERNAL_AUDIT') !== -1 || req.user.roles.indexOf('SALES') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];

module.exports.ensureSalesPurchaserProductionAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALESPURCHASER') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('PRODUCTION') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];


module.exports.ensureSalesPurchaserSalesAdmin = [
    passport.authenticate('bearer', {session: false}),
    (req, res, next) => {
        if (req.user.roles && (req.user.roles.indexOf('SALESPURCHASER') !== -1 || req.user.roles.indexOf('ADMIN') !== -1 || req.user.roles.indexOf('SALES') !== -1)) {
            next();
        } else {
            sendError(req, res);
        }
    },
];



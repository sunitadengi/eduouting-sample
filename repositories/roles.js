'use strict';

const pool = require('../routes/utils/database').getPool();
const bCrypt = require('bcrypt-nodejs');


exports.findByUserId = user => new Promise(function (callback, reject) {
    if (!user) {
        reject(new Error("Invalid Request"))
    } else {
        pool.getConnection(function (err, connection) {
            connection.query("Select u.roleType from cd_users_role as t join cd_role as u on t.roleId = u.id where t.userId = ?",
                [user.id], function (err, userRoles, fields) {
                    connection.release();
                    console.log('roles findByUserId', pool._freeConnections.indexOf(connection));
                    if (err)
                        reject(err);
                    else {
                        let roles = [];
                        for (const role of userRoles) {
                            roles.push(role.roleType);
                        }
                        user['roles'] = roles;
                        callback(user);
                    }
                });
        });
    }
});


exports.getAllRoleDetails = () => new Promise(function (callback, reject) {
    let sql = "Select * from cd_role where roleType != 'DRIVER' && roleType != 'CUSTOMER'";
    pool.getConnection(function (err, connection) {
        connection.query(sql, function (err, roleDetails, feilds) {
            connection.release();
            console.log('roles getAllRoleDetails', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                callback(roleDetails);
            }
        })
    });
});
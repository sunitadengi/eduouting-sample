'use strict';

const pool = require('../routes/utils/database').getPool();
const bCrypt = require('bcrypt-nodejs');

exports.createPassword = password => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("Insert into cd_password (passwordHash) VALUES (?)", [createHash(password)], function (err, password, fields) {
            connection.release();
            console.log('password createPassword', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                callback(password.insertId);
            }
        });
    });
});

exports.fetchPassword = passwordId => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("Select * from cd_password where id = ?", [passwordId], function (err, password) {
            connection.release();
            console.log('password fetchPassword', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                if (password) {
                    callback(password[0]);
                } else {
                    reject(new Error('Invalid password'))
                }
            }
        });
    });
});

exports.verifyPasswordIdByUserId = (req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("SELECT passwordId from cd_users WHERE id=?", [req.body.userId], function (err, userPasswordId, fields) {
            connection.release();
            console.log('password verifyPasswordIdByUserId', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                if (req.body.passwordId === userPasswordId[0].passwordId) {
                    callback("success");
                } else {
                    reject(new Error("Created a error while updateing the password"));
                }
            }
        });
    });
});

exports.resetPasswordByUserIdPasswordId = (req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("UPDATE cd_password SET passwordHash=? WHERE id=?", [createHash(req.body.confirmPassword), req.body.passwordId], function (err, password, fields) {
            connection.release();
            console.log('password resetPasswordByUserIdPasswordId', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                callback("success");
            }
        });
    });
});

exports.generateRandomPassword = (req) => new Promise(function (callback, reject) {
    var randomstring = Math.random().toString(36).slice(-6);
    pool.getConnection(function (err, connection) {
        let sql = "select name, passwordId from cd_users WHERE email=?";
        connection.query(sql, [req.params.customerEmail], function (err, result) {
            connection.release();
            console.log('password generateRandomPassword', pool._freeConnections.indexOf(connection));
            if (err) {
                reject(err);
            } else {
                if (result.length === 0) {
                    reject(new Error('Invalid Email Id'));
                } else {
                    req.randomPassword = randomstring;
                    req.passwordId = result[0].passwordId;
                    req.name = result[0].name;
                    let sendEmail = emailContent.getPasswordResetEmailToCustomer(req.params.customerEmail, req.name, req.randomPassword);
                    email.sendMail(sendEmail);
                    callback(req);
                }
            }
        });
    });
});

exports.updateRandomPasswordByPasswordId = (req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("UPDATE cd_password SET passwordHash=? WHERE id=?", [createHash(req.randomPassword), req.passwordId], function (err, password, fields) {
            connection.release();
            console.log('password updateRandomPasswordByPasswordId', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                callback("success");
            }
        });
    });
});

exports.getPasswordByUserId = (req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "select passwordId from cd_users WHERE id=?";
        connection.query(sql, [req.user.id], function (err, result) {
            connection.release();
            console.log('password getPasswordByUserId', pool._freeConnections.indexOf(connection));
            if (err) {
                reject(err);
            } else {
                req.passwordId = result[0].passwordId;
                callback();
            }
        });
    });
});

exports.updatePasswordByPasswordId = (req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("UPDATE cd_password SET passwordHash=? WHERE id=?", [createHash(req.params.confirmPassword), req.passwordId], function (err, password, fields) {
            connection.release();
            console.log('password updatePasswordByPasswordId', pool._freeConnections.indexOf(connection));
            if (err)
                reject(err);
            else {
                callback("success");
            }
        });
    });
});

let createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

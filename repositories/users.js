'use strict';

const pool = require('../routes/utils/database').getPool();
const bCrypt = require('bcrypt-nodejs');
const roles = require('./roles');
/**
 * Returns a user if it finds one, otherwise returns null if a user is not found.
 * @param   {String}   username - The unique user name to find
 * @param   {Function} done     - The user if found, otherwise returns undefined
 * @returns {Promise} resolved user if found, otherwise resolves undefined
 */

exports.findByUsername = username => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("SELECT * from cd_users where email = ? ", [username], function (error, user, feilds) {
            connection.release();
            console.log('users findByUsername', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error)
            } else {
                let userData = user[0];
                if (user.length && user) {
                    if (userData.isEnabled === 1) {
                        callback(userData);
                    } else {
                        reject(new Error('User is disabled'));
                    }

                } else {
                    reject(new Error('Invalid User'));
                }

            }
        });
    });

});

exports.getAllUsers = () => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "SELECT cd_users.id,cd_users.name,cd_users.email,cd_users.uniqueUserId, cd_users.passwordId, cd_users.phone,cd_users_details.accountNumber,cd_users_details.aadharNumber,cd_users_details.pan,cd_users_details.accountType,cd_users_details.bankName,cd_users_details.branchName,cd_users_details.ifsc,cd_users_details.address,cd_role.roleType,cd_role.description ,cd_sales_unit.name as salesUnitName FROM cd_users LEFT JOIN cd_users_details ON cd_users.id = cd_users_details.userId INNER JOIN cd_users_role ON cd_users.id = cd_users_role.userId INNER JOIN cd_role ON cd_users_role.roleId = cd_role.id left join  cd_sale_unit_user on cd_users.id = cd_sale_unit_user.userId left join cd_sales_unit on cd_sales_unit.id = cd_sale_unit_user.salesUnitId;";
        connection.query(sql, function (error, userData, feilds) {
            connection.release();
            console.log('users getAllUsers', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error)
            } else {
                callback(userData);
            }
        });
    });

});


exports.getAllSalesUsersDetails = () => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "select *, cd_users.id as userId from cd_users inner join cd_users_role on" +
            " cd_users_role.userId = cd_users.id inner join cd_role on cd_role.id = cd_users_role.roleId  where cd_role.roleType = 'SALES';";
        connection.query(sql, function (error, saleUsersUserData, feilds) {
            connection.release();
            console.log('users getAllSalesUsersDetails', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error)
            } else {
                callback(saleUsersUserData);
            }
        });
    });

});
exports.findById = id => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "SELECT cd_users.* from cd_users where id = ?";
        connection.query(sql, [id], function (error, userData, feilds) {
            connection.release();
            console.log('users findById', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error);
            } else {
                callback(userData[0])
            }
        });
    });
});

exports.getUserDetailsByUserId = id => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "SELECT cd_users.id,cd_users.name,cd_users.email,cd_users.phone, " +
            " cd_users_details.accountNumber,cd_users_details.aadharNumber,cd_users_details.pan, " +
            " cd_users_details.accountType,cd_users_details.bankName,cd_users_details.branchName, " +
            " cd_users_details.ifsc,cd_users_details.address,cd_role.roleType,cd_role.description , cd_sale_unit_user.salesUnitId" +
            " FROM cd_users INNER JOIN cd_users_details ON cd_users.id = cd_users_details.userId " +
            " INNER JOIN cd_users_role ON cd_users.id = cd_users_role.userId INNER JOIN cd_role ON " +
            " cd_users_role.roleId = cd_role.id  left join cd_sale_unit_user on  " +
            " cd_users.id = cd_sale_unit_user.userId left join  cd_sales_unit on cd_sales_unit.id = cd_sale_unit_user.salesUnitId " +
            " WHERE cd_users.id = ?;";
        connection.query(sql, [id], function (error, userData, feilds) {
            connection.release();
            console.log('users getUserDetailsByUserId', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error);
            } else {
                callback(userData[0])
            }
        });
    });
});

exports.findByToken = token => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("SELECT * from cd_users where id = (select userId from cd_auth_token where token = ?)",
            [token], function (error, user, feilds) {
                connection.release();
                console.log('users findByToken', pool._freeConnections.indexOf(connection));
                if (error) {
                    reject(error)
                } else {
                    callback(user[0])
                }
            });
    });
});

exports.createUser = (user, passwordId) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("Insert into cd_users (name, email, phone, passwordId) VALUES (?, ?, ?, ?)",
            [user.name, user.email, user.phone, passwordId], function (err, user, fields) {
                connection.release();
                console.log('users createUser', pool._freeConnections.indexOf(connection));
                if (err)
                    reject(err);
                else {
                    callback(user.insertId);
                }
            });
    })
});

exports.attachRoleToUser = (userId, roleType) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query('Insert into cd_users_role (userId, roleId) VALUES (?, (Select id from cd_role where roleType = ?))',
            [userId, roleType], function (err, insertResult) {
                connection.release();
                console.log('users attachRoleToUser', pool._freeConnections.indexOf(connection));
                if (err) {
                    reject(err)
                } else {
                    callback(userId);
                }
            })
    })
});


exports.getUsersCount = () => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "SELECT COUNT(*) as count,roleType from cd_users_role inner join cd_role on cd_role.id=cd_users_role.roleId \n" +
            "group by roleType ;";
        connection.query(sql, function (error, countRoleType, feilds) {
            connection.release();
            console.log('users getUsersCount', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error);
            } else {
                callback(countRoleType)
            }
        });
    });
});


exports.updateUser = (userId, req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("UPDATE cd_users SET name = ?, email = ?, phone = ? WHERE id = ?",
            [req.body.name, req.body.email, req.body.phone, userId], function (err, user, fields) {
                connection.release();
                console.log('users updateUser', pool._freeConnections.indexOf(connection));
                if (err)
                    reject(err);
                else {
                    callback(userId);
                }

            });
    })
});


exports.createUserDetails = (userId, userPersonalInfo) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("Insert into cd_users_details (aadharNumber, pan, accountNumber, accountType, bankName, branchName, ifsc, address, userId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [userPersonalInfo.aadharNumber,
                userPersonalInfo.pan,
                userPersonalInfo.accountNumber,
                userPersonalInfo.accountType,
                userPersonalInfo.bankName,
                userPersonalInfo.branchName,
                userPersonalInfo.ifsc,
                userPersonalInfo.address,
                userId], function (err, user, fields) {
                connection.release();
                console.log('users createUserDetails', pool._freeConnections.indexOf(connection));
                if (err)
                    reject(err);
                else {
                    callback(userId);
                }

            });
    })
});

exports.updateUserDetails = (userId, req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("UPDATE cd_users_details SET aadharNumber = ?, pan = ?, accountNumber = ?, accountType = ?, bankName = ?, branchName = ?, ifsc = ?, address = ?  WHERE userId = ? ",
            [req.body.aadharNumber, req.body.pan, req.body.accountNumber, req.body.accountType, req.body.bankName, req.body.branchName, req.body.ifsc, req.body.address, userId], function (err, user, fields) {
                connection.release();
                console.log('users updateUserDetails', pool._freeConnections.indexOf(connection));
                if (err)
                    reject(err);
                else {
                    callback(user);
                }

            });
    })
});

exports.updateUserByUserId = (req) => new Promise(function (callback, reject) {
    let sql = "UPDATE  cd_users set name = ? , email = ? , phone = ? WHERE id = ?";
    pool.getConnection(function (err, connection) {
        connection.query(sql, [req.body.name,
                req.body.email,
                req.body.phone,
                req.body.userId
            ],
            function (err, user) {
                connection.release();
                console.log('users updateUserByUserId', pool._freeConnections.indexOf(connection));
                if (err) {
                    reject(err)
                } else {
                    callback(user)
                }

            });
    });
});

exports.updateUserRoleByUserId = (req) => new Promise(function (callback, reject) {
    let sql = "UPDATE  cd_users_role SET roleId = (Select id from cd_role where roleType = ?)  WHERE userId = ?";
    pool.getConnection(function (err, connection) {
        connection.query(sql, [req.body.roleType,
                req.body.userId
            ],
            function (err, user) {
                connection.release();
                console.log('users updateUserRoleByUserId', pool._freeConnections.indexOf(connection));
                if (err) {
                    reject(err)
                } else {
                    callback(user)
                }

            });
    });
});


exports.getPreviouslyAddedUser = (req) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("select * from cd_users left join cd_users_role on cd_users.id = cd_users_role.userId left join cd_role on cd_users_role.roleId = cd_role.id where cd_users.id=(SELECT MAX(id) FROM cd_users)",
            function (err, userData, fields) {
                connection.release();
                console.log('users getPreviouslyAddedUser', pool._freeConnections.indexOf(connection));
                if (err)
                    reject(err);
                else {
                    callback(userData);
                }
            });
    })
});

exports.getAlluserId = () => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        connection.query("select * from cd_users",
            function (err, users, fields) {
                connection.release();
                console.log('users getAlluserId', pool._freeConnections.indexOf(connection));
                if (err)
                    reject(err);
                else {
                    callback(users);
                }
            });
    })
});

exports.getUserNameByUserId = (id) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        let sql = "SELECT name from cd_users WHERE id = ?";
        connection.query(sql, [id], function (error, userData, feilds) {
            connection.release();
            console.log('users getUserNameByUserId', pool._freeConnections.indexOf(connection));
            if (error) {
                reject(error)
            } else {
                callback(userData[0]);
            }
        });
    });

});

exports.attachCustomerRoleToUser = (userId) => new Promise(function (callback, reject) {
    pool.getConnection(function (err, connection) {
        var customer = 'CUSTOMER';
        connection.query('Insert into cd_users_role (userId, roleId) VALUES (?, (Select id from cd_role where roleType = ?))',
            [userId, customer], function (err, insertResult) {
                connection.release();
                console.log('users attachCustomerRoleToUser', pool._freeConnections.indexOf(connection));
                if (err) {
                    reject(err)
                } else {
                    callback(userId);
                }
            })
    })
});
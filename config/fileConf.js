var multer = require("multer");
var path = require("path");
var fs = require('fs');
var FileSize = 10*1024*1024; // Max File Size 10 MB default

module.exports.uploadFile = function uploadToS3(uploadFileSize=FileSize) {
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, path.resolve(__dirname + '../../../ariant-veg-files/uploads'))
        },
        filename: function (req, file, cb) {
            cb(null, Date.now().toString()+path.extname(file.originalname))
        }
    });

    var fileFilter = function(req,file,cb){
        if (file.mimetype === "image/jpeg"||file.mimetype==="image/png"|| file.mimetype === "image/jpg" || file.mimetype === "application/pdf") {
            cb(null,true);
        } else {
            var err = new TypeError("Invalid File Type. Only Images/Pdf are allowed");
            cb(err,false);
        }
    };

    return multer({
        storage:storage,
        fileFilter:fileFilter,
        limits:{
            fileSize: uploadFileSize
        }
    });
};


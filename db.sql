/* Drop ariantveg DB if it exists */
drop database if exists `ariantveg`;
CREATE DATABASE `ariantveg`;
USE `ariantveg`;
/* OAuth Table*/
CREATE TABLE `cd_auth_client` (
    `id` int(11) NOT NULL,
    `effectiveDate` date NOT NULL DEFAULT '1900-01-01',
    `expiryDate` date NOT NULL DEFAULT '3000-01-01',
    `name` varchar(255) NOT NULL,
    `clientId` varchar(255) NOT NULL,
    `clientSecret` varchar(255) NULL,
    `redirectUri` varchar(255) NOT NULL
);

ALTER TABLE `cd_auth_client`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE `u1_auth_client` (`clientId`),
    ADD INDEX `i1_auth_client` (`clientId`);

ALTER TABLE `cd_auth_client`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cd_auth_client`
    AUTO_INCREMENT = 101;
-- User Password

CREATE TABLE `cd_password` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `passwordHash` VARCHAR(4000) NOT NULL ,
    PRIMARY KEY (`id`)
);
ALTER TABLE `cd_password`
    AUTO_INCREMENT = 101;
/*User Table*/
CREATE TABLE `cd_users` (
    `id` int(11) NOT NULL,
    `name` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `phone` varchar(255) NULL,
    `passwordId` int(11) NOT NULL,
    `isEnabled` boolean  default '1',
    `uniqueUserId` varchar(255) NULL
);

ALTER TABLE `cd_users`
    ADD PRIMARY KEY (`id`),
    ADD KEY `passwordId` (`passwordId`),
    ADD UNIQUE `u1_users` (`email`),
    ADD INDEX `i1_users` (`name`),
    ADD INDEX `i2_users` (`email`);

ALTER TABLE `cd_users`
    ADD CONSTRAINT `fk1_users` FOREIGN KEY (`passwordId`) REFERENCES `cd_password` (`id`);

ALTER TABLE `cd_users`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cd_users`
    AUTO_INCREMENT = 101;

CREATE TABLE `cd_users_details` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `aadharNumber` BIGINT(15),
    `pan` VARCHAR(255),
    `accountNumber` VARCHAR(255),
    `accountType` VARCHAR(255),
    `bankName` VARCHAR(255),
    `branchName` VARCHAR(255),
    `ifsc` VARCHAR(255),
    `address` VARCHAR(255),
    `userId`  INT(15) NOT NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

 ALTER TABLE `cd_users_details`
     AUTO_INCREMENT = 101,
     ADD CONSTRAINT `fk1_cd_users_details` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);


CREATE TABLE `cd_auth_token` (
    `id` int(11) NOT NULL,
    `effectiveTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `expiryTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `token` varchar(767) NOT NULL,
    `userId` int(11) NOT NULL,
    `clientId` int(11) NOT NULL
);
ALTER TABLE `cd_auth_token`
    ADD PRIMARY KEY (`id`),
    ADD KEY `userId` (`userId`),
    ADD KEY `clientId` (`clientId`),
    ADD INDEX `i1_users_auth_token` (`token`,`clientId`),
    ADD INDEX `i2_users_auth_token` (`token`,`userId`);

ALTER TABLE `cd_auth_token`
    ADD CONSTRAINT `fk1_auth_token` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

ALTER TABLE `cd_auth_token`
    ADD CONSTRAINT `fk2_auth_token` FOREIGN KEY (`clientId`) REFERENCES `cd_auth_client` (`id`);

ALTER TABLE `cd_auth_token`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cd_auth_token`
    AUTO_INCREMENT = 101;

CREATE TABLE `cd_refresh_token` (
    `id` int(11) NOT NULL,
    `effectiveTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `expiryTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `refreshToken` varchar(1023) NOT NULL,
    `userId` int(11) NOT NULL,
    `clientId` int(11) NOT NULL
);
ALTER TABLE `cd_refresh_token`
    ADD PRIMARY KEY (`id`),
    ADD KEY `userId` (`userId`),
    ADD KEY `clientId` (`clientId`),
    ADD INDEX `i1_users_refresh_token` (`refreshToken`,`clientId`),
    ADD INDEX `i2_users_refresh_token` (`refreshToken`,`userId`);

ALTER TABLE `cd_refresh_token`
    ADD CONSTRAINT `fk1_users_refresh_token` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

ALTER TABLE `cd_refresh_token`
    ADD CONSTRAINT `fk2_users_refresh_token` FOREIGN KEY (`clientId`) REFERENCES `cd_auth_client` (`id`);

ALTER TABLE `cd_refresh_token`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cd_refresh_token`
    AUTO_INCREMENT = 101;
/* Role */
CREATE TABLE `cd_role` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `roleType` VARCHAR(255) NOT NULL ,
    `description` VARCHAR(4000) NOT NULL ,
    PRIMARY KEY (`id`)
);
ALTER TABLE `cd_role`
    AUTO_INCREMENT = 101;

ALTER TABLE `cd_role`
    ADD UNIQUE `u1_role` (`roleType`);
/* User Role Mapping */

CREATE TABLE `cd_users_role` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `userId` INT NOT NULL ,
    `roleId` INT NOT NULL ,
    PRIMARY KEY (`id`)
);
ALTER TABLE `cd_users_role`
    AUTO_INCREMENT = 101;

ALTER TABLE `cd_users_role`
    ADD UNIQUE `u1_users_role` (`userId`, `roleId`),
    ADD INDEX `i1_users_role` (`userId`),
    ADD INDEX `i2_users_role` (`roleId`);

ALTER TABLE `cd_users_role`
    ADD CONSTRAINT `fk1_users_role` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_users_role` FOREIGN KEY (`roleId`) REFERENCES `cd_role` (`id`);


CREATE TABLE `cd_file` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`originalFilename` VARCHAR(255),
	`filename` VARCHAR(255),
	`createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`)
);

ALTER TABLE `cd_file`
	AUTO_INCREMENT = 101;

CREATE TABLE `cd_payment_details` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `paymentMode` VARCHAR(255), # CASH, CHEQUE, NET_BANKING
    `paymentType` VARCHAR(255), # ADVANCE, FINAL_SETTLEMENT
    `amount` float(15)  NULL,
    `transactionNumber` VARCHAR(255),
    `accountNumber` VARCHAR(255),
    `accountType` VARCHAR(255),
    `bankName` VARCHAR(255),
    `branchName` VARCHAR(255),
    `ifscCode` VARCHAR(50),
    `chequeNumber` VARCHAR(255),
    `chequeDate` DATE,
    `chequeCleared` BOOLEAN,
    `accountHolderName` VARCHAR(255),
    `transactionCopyId` int(11),
    `uniqueTransanctionId` VARCHAR(255) NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_payment_details`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_payment_details` FOREIGN KEY (`transactionCopyId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `cd_products` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(767) NOT NULL,
    `uom` VARCHAR(255) NOT NULL,
    `categoryType` VARCHAR(255) NOT NULL ,
    `preservationMethods` VARCHAR(255),
    `gst` float(20) NULL,
    `isEnable` boolean  default '1',
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `productImageId` INT(11) NULL,
    `uniqueProductId` VARCHAR(255) NULL,
    `hsnCode` VARCHAR(255) NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_products`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_product_image_id` FOREIGN KEY (`productImageId`) REFERENCES `cd_file` (`id`);


CREATE TABLE `cd_products_gallery_file`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `fileId` INT(11) NOT NULL,
    `productId` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_products_gallery_file`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_product_gallery_image_id` FOREIGN KEY (`fileId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `cd_online_price`(
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11) NOT NULL,
    `date` date NOT NULL,
    `price` float(20) NOT NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_online_price`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_online_price` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`);

CREATE TABLE `cd_customer_details` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `customerType` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NULL,
    `phone` VARCHAR(255) NOT NULL,
    `address` VARCHAR(255) NOT NULL,
    `deliveryAddress` VARCHAR(255) NOT NULL,
    `country` VARCHAR(255) NULL,
    `state` VARCHAR(255) NULL,
    `pincode` INT(11) NOT NULL,
    `deliveryPincode` INT(11) NOT NULL,
    `companyName` VARCHAR(255) NULL,
    `companyPhoneNumber` BIGINT(11) NULL,
    `companyEmail` VARCHAR(255) NULL,
    `gstin` VARCHAR(255) NULL,
    `pan` VARCHAR(255) NULL,
    `isB2B` BOOLEAN NOT NULL,
    `pocName` VARCHAR(255)  NULL,
    `pocNumber` VARCHAR(255)  NULL,
    `alternatePocName` VARCHAR(255)  NULL,
    `alternatePocNumber` VARCHAR(255)  NULL,
    `agreementCopyId` INT(11)  NULL,
    `uniqueCustomerId` VARCHAR(255)  NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_customer_details`
    ADD UNIQUE `c1_cd_customer_details` (`email`),
    ADD UNIQUE `c2_cd_customer_details` (`phone`);

ALTER TABLE `cd_customer_details`
    ADD CONSTRAINT `fk1_cd_customer_details` FOREIGN KEY (`agreementCopyId`) REFERENCES `cd_file` (`id`);

ALTER TABLE `cd_customer_details`
	AUTO_INCREMENT = 101;
	
CREATE TABLE `cd_config_type` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`name` VARCHAR(255) NOT NULL ,
	`typeEnum` VARCHAR(255) NOT NULL,
	`description` VARCHAR(4000) NOT NULL ,
	PRIMARY KEY (`id`)
);

ALTER TABLE `cd_config_type`
    AUTO_INCREMENT = 101;

CREATE TABLE `cd_config` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `configTypeId` INT(11) NOT NULL ,
    `amount` INT(11),
    `updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_config`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_config` FOREIGN KEY (`configTypeId`) REFERENCES `cd_config_type` (`id`);

CREATE TABLE `cd_stock`(
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11) NOT NULL,
    `vendorStock` INT(11)  default 0,
    `warehouseStock` INT(11)  default 0,
    `readyStock` INT(11)  default 0,
    `wastage` INT(11)  default 0,
    `shrinkage` INT(11)  default 0,
    `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_stock`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_stock` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`);

CREATE TABLE `cd_wastage_shrinkage`(
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11),
    `wastage` INT(11) NULL,
    `shrinkage` INT(11) NULL,
    `userId` INT(11),
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_wastage_shrinkage`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_wastage_shrinkage` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`),
    ADD CONSTRAINT `fk2_cd_wastage_shrinkage` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_package` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11), 
    `stockAvailability` INT(11)  default 0, 
    `updatedDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_package`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_package` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`);

 CREATE TABLE `cd_employee` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`name` VARCHAR(255) NOT NULL,
	`phone` VARCHAR(25) NOT NULL,
	`email` VARCHAR(25) NOT NULL,
	`location` VARCHAR(255) NOT NULL,
	`employeeId` VARCHAR(255) NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `cd_employee`
    AUTO_INCREMENT = 101;

CREATE TABLE `cd_package_usage` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11) NOT NULL, 
    `usedQuantity` INT(11), 
    `createdById` INT(11) NOT NULL,
    `empId` INT(11) NOT NULL,
    `updatedDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_package_usage`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_package_usage` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`),
    ADD CONSTRAINT `fk2_cd_package_usage` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk3_cd_package_usage` FOREIGN KEY (`empId`) REFERENCES `cd_employee` (`id`);

CREATE TABLE `cd_sales_unit` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`name` VARCHAR(255) NULL,
	`address` VARCHAR(255) NULL,
	`phone` VARCHAR(15) NULL,
	`incentivePercentage` FLOAT(15),
	`isEnabled` boolean  default '1',
	`createdById` INT(11) NULL,
	`uniqueSalesUnitId` VARCHAR(255) NULL,
	`createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `cd_sales_unit`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_sales_unit` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_sale_unit_user` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `userId` INT(11) NULL,
    `salesUnitId` INT(11) NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_sale_unit_user`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_sale_unit_user` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_cd_sale_unit_user` FOREIGN KEY (`salesUnitId`) REFERENCES `cd_sales_unit` (`id`);

CREATE TABLE `cd_banner` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `text` VARCHAR(255) NULL,
    `bannerImageId` INT(11) NULL,
    `userId` INT(11) NULL,
    `updatedTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_banner`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_banner` FOREIGN KEY (`bannerImageId`) REFERENCES `cd_file` (`id`),
    ADD CONSTRAINT `fk2_cd_banner` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_notifications` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`message` VARCHAR(767),
	`userId`  INT(11) NOT NULL,
	`createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`isDeleted` BOOLEAN  DEFAULT '0',
	`isViewed`BOOLEAN  DEFAULT '0',
	`url` VARCHAR(255),
	PRIMARY KEY (`id`)
);

ALTER TABLE `cd_notifications`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_cd_notifications` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_officers` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `userId`  INT(11) NOT NULL,
    `fromDate` date NOT NULL,
    `fromTime` time NOT NULL,
    `toDate` date NOT NULL,
    `toTime` time NOT NULL,
    `saleUnitId` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_officers`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_officers` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_cd_officers` FOREIGN KEY (`saleUnitId`) REFERENCES `cd_sales_unit` (`id`);

CREATE TABLE `cd_online_price_history`(
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`productId` INT(11) NOT NULL,
	`date` date NOT NULL,
	`price` float(20) NOT NULL,
	`userId` INT(11),
	`createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `cd_online_price_history`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_cd_online_price_history` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`),
	ADD CONSTRAINT `fk2_cd_online_price_history` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_bill` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `customerName` VARCHAR(255) NULL, 
    `customerNumber` VARCHAR(255) NULL, 
    `billDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deliveryAddress` VARCHAR(255) NOT NULL,
    `billingAddress` VARCHAR(255) NOT NULL,
	`deliveryCharges` float(25) NULL,
	`gstin` VARCHAR(255) NULL,
    `userId` INT(11) NOT NULL,
    `totalPayableAmount` FLOAT(25) NULL,
    `uniqueBillId` VARCHAR(255) NULL, 
     PRIMARY KEY (`id`)
);

ALTER TABLE `cd_bill`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_cd_bill` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_bill_items` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`productId` INT(10) NOT NULL,
	`quantity` BIGINT(11) NOT NULL,
	`billId` INT(11) NOT NULL,
	`price` FLOAT(11) NOT NULL,
	`gst` float(10),
	`subTotal` FLOAT(11) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `cd_bill_items`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_cd_bill_items` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`),
	ADD CONSTRAINT `fk2_cd_bill_items` FOREIGN KEY (`billId`) REFERENCES `cd_bill` (`id`);

CREATE TABLE `delivery_charges` (
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `km` varchar(45) NULL,
    `charges` varchar(255) NULL
);
ALTER TABLE `delivery_charges`
    AUTO_INCREMENT = 101;

CREATE TABLE `ex_expense_details` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `expenseDate` DATE NOT NULL,
    `expenseBy` VARCHAR(255) NOT NULL,
    `expenseByDesignation` VARCHAR(255) NOT NULL,
    `expenseByPhone` BIGINT(11) NOT NULL,
    `expenseCategory` VARCHAR(255) NOT NULL, # LOGISTICS, MISC, INFRASTRUCTURE, OTHERS
    `expenseType` VARCHAR(255) NOT NULL, # PRODUCTS, SERVICES, PETTY_CASH, OTHERS
    `description` VARCHAR(255) NOT NULL,
    `vendorType` VARCHAR(255) NOT NULL, # INDIVIDUAL, COMPANY
    `companyName` VARCHAR(255) NULL,
    `companyPhoneNumber` BIGINT(11) NULL,
    `gst` float(15) NULL,
    `isIaVerified` BOOLEAN  DEFAULT 0,
    `gstin` VARCHAR(255) NULL,
    `personName` VARCHAR(255) NULL,
    `personPhoneNumber` BIGINT(11) NULL,
    `totalAmount` float(15) NOT NULL,
    `comment` VARCHAR(255) NULL,
    `iaErrors` VARCHAR(255) NULL,
    `gstYes` BOOLEAN   DEFAULT 0,
    `isClosed` BOOLEAN   DEFAULT 0,
    `status` VARCHAR(255) NULL, # COMPLETED, PENDING
    `createdById` INT(11) NOT NULL,
    `billCopyId` INT(11),
    `uniqueExpenseId` VARCHAR(255) NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `ex_expense_details`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_expense_created_by_id` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_expense_bill_copy_id` FOREIGN KEY (`billCopyId`) REFERENCES `cd_file` (`id`);
  
CREATE TABLE `ex_expense_transactions` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `expenseId` INT(11) NOT NULL,
    `paymentDetailsId` INT(11) NOT NULL,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `ex_expense_details`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_ex_expense_details` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_ex_expense_details` FOREIGN KEY (`billCopyId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `ex_petty_cash_withdraw` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `withdrawalAmount` INT(11) NOT NULL,
    `withdrawalBankName` VARCHAR(255),
    `accountNumber` VARCHAR(255),
    `transctionId` VARCHAR(11),
    `withdrawalBy` varchar(255) NULL,
    `date` DATE,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `ex_petty_cash_withdraw`
    AUTO_INCREMENT = 101;
CREATE TABLE `lg_driver_details`(
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`licenseNumber` VARCHAR(255) NOT NULL,
	`licenseExpireDate` date ,
	`yellowBadgeNumber` VARCHAR(255) NOT NULL,
	`yellowBadgeExpireDate` VARCHAR(255),
	`driverId` INT(11) NOT NULL UNIQUE,
	`isEnabled` BOOLEAN  DEFAULT 1,
	`licenseCopyId` INT(11),
	`yellowBadgeCopyId` INT(11),
	`createdById` INT(11) NOT NULL,
	`createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `lg_driver_details`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_lg_driver_details` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`),
	ADD CONSTRAINT `fk2_lg_driver_details` FOREIGN KEY (`driverId`) REFERENCES `cd_users` (`id`),
	ADD CONSTRAINT `fk3_lg_driver_details` FOREIGN KEY (`licenseCopyId`) REFERENCES `cd_file` (`id`),
	ADD CONSTRAINT `fk4_lg_driver_details` FOREIGN KEY (`yellowBadgeCopyId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `lg_vehicle_details`(
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`name` VARCHAR(255) NOT NULL,
	`vehicleNumber` VARCHAR(255) NOT NULL,
	`chassisNumber`VARCHAR(255) NOT NULL,
	`engineNumber` VARCHAR(255) NOT NULL,
	`rcNumber` VARCHAR(255) NOT NULL,
	`vehicleType` VARCHAR(255)  NOT NULL, -- TWO_WHEELER, FOUR_WHEELER
	`capacity` float(20) NOT NULL,
	`insuranceCompanyName` VARCHAR(255) NULL,
	`insuranceNumber`VARCHAR(255) NOT NULL,
	`insuranceAmount` float(20) NOT NULL,
	`insuranceExpireDate` date ,
	`idv` FLOAT(11) NULL,
	`fitnessCertificateNumber` VARCHAR(255) NOT NULL,
	`fitnessCertificateExpireDate` DATE ,
	`emissionTestNumber` VARCHAR(255) NOT NULL,
	`emissionTestExpireDate` VARCHAR(255) ,
	`isEnabled` BOOLEAN  DEFAULT 1,
	`rcCopyId` INT(11),
	`insuranceCopyId` INT(11),
	`fitnessCertificationCopyId` INT(11),
	`emissionTestCopyId` INT(11),
	`createdById` INT(11) NOT NULL,
	`uniqueVehicleId` VARCHAR(255) NULL,
	`createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `lg_vehicle_details`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_lg_vehicle_details` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`),
	ADD CONSTRAINT `fk2_lg_vehicle_details` FOREIGN KEY (`rcCopyId`) REFERENCES `cd_file` (`id`),
	ADD CONSTRAINT `fk3_lg_vehicle_details` FOREIGN KEY (`insuranceCopyId`) REFERENCES `cd_file` (`id`),
	ADD CONSTRAINT `fk4_lg_vehicle_details` FOREIGN KEY (`fitnessCertificationCopyId`) REFERENCES `cd_file` (`id`),
	ADD CONSTRAINT `fk5_lg_vehicle_details` FOREIGN KEY (`emissionTestCopyId`) REFERENCES `cd_file` (`id`);
       
CREATE TABLE `lg_vehicle_assignment` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`vehicleId` INT(10) NOT NULL UNIQUE,
	`driverId` INT(10) NOT NULL UNIQUE,
	`createdById` INT(11) NOT NULL,
	`createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `lg_vehicle_assignment`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_lg_vehicle_assignment` FOREIGN KEY (`vehicleId`) REFERENCES `lg_vehicle_details` (`id`),
	ADD CONSTRAINT `fk2_lg_vehicle_assignment` FOREIGN KEY (`driverId`) REFERENCES `lg_driver_details` (`id`), 
	ADD CONSTRAINT `fk3_lg_vehicle_assignment` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`);

CREATE TABLE `lg_vehicle_service` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`vehicleId` INT(10) NOT NULL,
	`serviceGarageName` VARCHAR(255) NOT NULL,
	`dateOfService` DATE ,
	`serviceDescription` VARCHAR(255) NULL, 
	`dateOfNextService` DATE ,
	`isOilChanged` BOOLEAN NOT NULL,
	`nextServiceComments` VARCHAR(255),
	`nextServiceKM` float(10) NULL,
	`amount` float(15) NULL,
	`status` varchar(255),
	`createdById` INT(11) NOT NULL,
	`createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `lg_vehicle_service`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_vehicle_service` FOREIGN KEY (`vehicleId`) REFERENCES `lg_vehicle_details` (`id`),
	ADD CONSTRAINT `fk2_vehicle_service` FOREIGN KEY (`createdById`) REFERENCES `cd_users` (`id`); 
 
CREATE TABLE `sc_visitor_management` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL,
    `phone` VARCHAR(25) NOT NULL,
    `noOfVisitors` INT(11) NOT NULL,
    `purpose` VARCHAR(255) NOT NULL,
    `toMeet` VARCHAR(255) NOT NULL,
    `vehicleType`  VARCHAR(255) NULL,
    `vehicleNumber`  VARCHAR(255) NULL,
    `fromLocation` VARCHAR(255)  NULL,
    `inTime`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `outTime` DATETIME ,
    `visitorHandlerId` INT(11) NOT NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `uniqueVisitorId` VARCHAR(255)  NULL,
    PRIMARY KEY (id)
);

ALTER TABLE `sc_visitor_management`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT fk1_sc_visitor_management FOREIGN KEY (visitorHandlerId) REFERENCES cd_users (id);

CREATE TABLE `sc_emp` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL,
    `empId` VARCHAR(255) NULL,
    `inTime`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `outTime` DATETIME,
    `handlerId` INT(11) NOT NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

ALTER TABLE `sc_emp`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT fk1_sc_emp FOREIGN KEY (handlerId) REFERENCES cd_users (id);

CREATE TABLE `sc_grn` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL,
    `phone` VARCHAR(25) NOT NULL,
    `noOfVisitors` INT(11) NOT NULL,
    `location` VARCHAR(255) NOT NULL,
    `companyName` VARCHAR(255) NULL,
    `companyPhone` VARCHAR(25) NULL,
    `vehicleType`  VARCHAR(255) NULL,
    `vehicleNumber`  VARCHAR(255) NULL,
    `materialDescription` VARCHAR(255) NOT NULL,
    `inTime`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `outTime` TIMESTAMP NULL,
    `handlerId` INT(11) NOT NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `uniqueGrnId`  VARCHAR(255) NULL,
    PRIMARY KEY (id)
);

ALTER TABLE `sc_grn`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT fk1_sc_grn FOREIGN KEY (handlerId) REFERENCES cd_users (id);

CREATE TABLE `cd_orders` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `customerId` INT(10) NOT NULL,
    `orderDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deliveryDate` DATE NOT NULL,
    `deliveryCharges` float(25) NULL,
    `comment` VARCHAR(255),
    `userId` INT(11) NOT NULL,
    `iaErrors` VARCHAR(255) NULL, 
    `isVerified` BOOLEAN NOT NULL DEFAULT 0, 
    `isClose` BOOLEAN NOT NULL DEFAULT 0,
    `totalPayableAmount` FLOAT(25) NOT NULL,
    `isDispatched` BOOLEAN NOT NULL DEFAULT 0,
    `status` VARCHAR(255) DEFAULT 'PENDING',
    `uniqueOrderId` VARCHAR(255) NULL, 
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`)
);

ALTER TABLE `cd_orders`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_cd_orders` FOREIGN KEY (`customerId`) REFERENCES `cd_customer_details` (`id`),
	ADD CONSTRAINT `fk2_cd_orders` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `order_items` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`productId` INT(10) NOT NULL,
	`quantity` BIGINT(11) NOT NULL,
	`ordersId` INT(11) NOT NULL,
	`price` FLOAT(11) NOT NULL,
    `gst` float(10),
	`subTotal` FLOAT(11) NOT NULL,
	`comment` VARCHAR(255),
	`isPacked` BOOLEAN  DEFAULT 0,
	`isCancelled` BOOLEAN  DEFAULT 0,
	`createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `order_items`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_order_items` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`),
	ADD CONSTRAINT `fk2_order_items` FOREIGN KEY (`ordersId`) REFERENCES `cd_orders` (`id`);

CREATE TABLE `customer_payment` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`ordersId` INT(10) NOT NULL,
	`paymentDetailsId` INT(11),
	`createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

ALTER TABLE `customer_payment`
	AUTO_INCREMENT = 101,
	ADD CONSTRAINT `fk1_customer_payment` FOREIGN KEY (`ordersId`) REFERENCES `cd_orders` (`id`),
	ADD CONSTRAINT `fk2_customer_payment` FOREIGN KEY (`paymentDetailsId`) REFERENCES `cd_payment_details` (`id`);

CREATE TABLE `online_contact_us` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`customer_id` INT(11) NOT NULL,
	`message` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);

 ALTER TABLE `online_contact_us`
	 AUTO_INCREMENT = 101,
	 ADD CONSTRAINT `fk1_online_contact_us` FOREIGN KEY (`customer_id`) REFERENCES `cd_customer_details` (`id`);

CREATE TABLE `deliverable_pincode` (
	`id` INT(11) NOT NULL AUTO_INCREMENT ,
	`area` INT(11) NOT NULL,
	`is_free` boolean NOT NULL,
	`pincode_range` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `deliverable_pincode`
	AUTO_INCREMENT = 101;
CREATE TABLE `vms_purchase_products` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productName` VARCHAR(255) NOT NULL UNIQUE, 
    `gstPercentage` float(10) DEFAULT '0.0',
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `uniquePurchaseProductId` VARCHAR(255) NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_purchase_products`
    AUTO_INCREMENT = 101;
    
CREATE TABLE `vms_vendor_personal_details` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `vendorType` VARCHAR(255) NOT NULL,
    `companyName` VARCHAR(255) NOT NULL,
    `companyAddress` VARCHAR(255) NULL,
    `companyEmail` VARCHAR(255) NULL,
    `companyPan` VARCHAR(255) NULL,
    `gstin` VARCHAR(255)  NULL,
    `companyContactNumber` VARCHAR(25) NOT NULL,
    `pocName` VARCHAR(255) NULL,
    `pocNumber` VARCHAR(25)  NULL,  
    `alternatePocName` VARCHAR(255)  NULL,
    `alternatePocNumber` VARCHAR(25)  NULL,
    `paytmNumber` VARCHAR(25) NULL,
    `whatsapp` VARCHAR(25) NULL,
    `googlePay` VARCHAR(25) NULL,
    `creditAmount` FLOAT(25)   DEFAULT '0',
    `outstandingAmount` FLOAT(25)  DEFAULT '0',
    `agreementCopyId` INT(11),
    `userId` INT(11) NOT NULL,
    `isEnabled` BOOLEAN  DEFAULT 1,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `uniqueVendorId` VARCHAR(25) NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_vendor_personal_details`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_vms_vendor_personal_details` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_vms_vendor_personal_details` FOREIGN KEY (`agreementCopyId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `vms_vendor_bank_details` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `accountHolderName` VARCHAR(255) NOT NULL,
    `accountNumber` VARCHAR(255) NOT NULL,
    `accountType` VARCHAR(255) NOT NULL,
    `ifscCode` VARCHAR(255) NOT NULL,
    `branchName` VARCHAR(255) NOT NULL,
    `micrNumber` VARCHAR(255) NULL,
    `vendorId` INT(11) NOT NULL,
    `upiId` VARCHAR(255) NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_vendor_bank_details`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_vms_vendor_bank_details` FOREIGN KEY (`vendorId`) REFERENCES `vms_vendor_personal_details` (`id`);

CREATE TABLE `vms_vendor_products` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `vmsPurchaseProductsId` INT(15) NOT NULL,
    `maxSupplyQuantity` INT(11) NULL,
    `vendorId` INT(11) NOT NULL,
    `productCategory` VARCHAR(255) NOT NULL, 
    `uom` VARCHAR(255) NOT NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_vendor_products`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_vms_vendor_products` FOREIGN KEY (`vmsPurchaseProductsId`) REFERENCES `vms_purchase_products` (`id`),
    ADD CONSTRAINT `fk2_vms_vendor_products` FOREIGN KEY (`vendorId`) REFERENCES `vms_vendor_personal_details` (`id`);

CREATE TABLE `vms_vendor_purchase_order` ( 
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `vendorId` INT(15) NOT NULL, 
    `orderedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `advancedAmount` FLOAT(25)  DEFAULT '0',
    `comments` VARCHAR(255)  NULL,
    `iaErrors` VARCHAR(255) NULL, 
    `isVerified` BOOLEAN  DEFAULT 0, 
    `finalAmount` FLOAT(25)  NULL,
    `transportType` VARCHAR(255) NULL,
    `expectedDateOfDelivery` DATETIME  NULL,
    `expectedTime` TIME NULL,
    `status` VARCHAR(255) DEFAULT 'PENDING',
    `isClosed` BOOLEAN  DEFAULT 0,
    `isCancelled` BOOLEAN  DEFAULT 0,
    `creditIncluded` FLOAT(25)   DEFAULT '0',
    `outstandingIncluded` FLOAT(25)  DEFAULT '0',
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `uniquePurchaseOrderId` VARCHAR(255) NULL,
    `billCopyId` INT(11),
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_vendor_purchase_order`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_vms_vendor_purchase_order` FOREIGN KEY (`vendorId`) REFERENCES `vms_vendor_personal_details` (`id`),
    ADD CONSTRAINT `fk2_vms_vendor_purchase_order` FOREIGN KEY (`billCopyId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `vms_vendor_payments` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `purchaseOrderId` INT(11) NOT NULL,
    `paymentDetailsId` INT(11) NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_vendor_payments`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_vms_vendor_payments` FOREIGN KEY (`purchaseOrderId`) REFERENCES `vms_vendor_purchase_order` (`id`),
    ADD CONSTRAINT `fk2_vms_vendor_payments` FOREIGN KEY (`paymentDetailsId`) REFERENCES `cd_payment_details` (`id`);

CREATE TABLE `vms_vendor_purchase_order_item` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `vendorProductId` INT(10) NOT NULL,
    `quantity` INT(15) NOT NULL,
    `pricePerUnit` FLOAT(11),
    `subTotal` FLOAT(11)  DEFAULT '0',
    `gstPercentage` INT(11) DEFAULT '0',
    `purchaseOrderId` INT(10) NOT NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `vms_vendor_purchase_order_item`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_vms_vendor_purchase_order_item` FOREIGN KEY (`purchaseOrderId`) REFERENCES `vms_vendor_purchase_order` (`id`),
    ADD CONSTRAINT `fk2_vms_vendor_purchase_order_item` FOREIGN KEY (`vendorProductId`) REFERENCES `vms_vendor_products` (`vmsPurchaseProductsId`);



CREATE TABLE `driver_expense` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `driverId` INT(11) NOT NULL,
    `expenseDate` DATE NULL,
    `expenseTime` TIME NOT NULL,
    `expenseAmount` float(25) NOT NULL,
    `expenseType` VARCHAR(255) NOT NULL,
    `expenseDescription` VARCHAR(255) NOT NULL ,
    `paymentMode` VARCHAR(255) NOT NULL,
    `status` VARCHAR(255) DEFAULT 'PENDING',
    `iaErrors` VARCHAR(255) ,
    `isClosed` BOOLEAN   DEFAULT '0',
    `isVerified` BOOLEAN  DEFAULT '0',
    `billCopyId` INT(11), 
    `uniqueDriverExpenseId` VARCHAR(255) NULL,
    `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `driver_expense`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_driver_expense` FOREIGN KEY (`driverId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_driver_expense` FOREIGN KEY (`billCopyId`) REFERENCES `cd_file` (`id`);


CREATE TABLE `su_sales_unit_orders` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `orderDate` TIMESTAMP  DEFAULT CURRENT_TIMESTAMP,
    `suUserId` INT(11) NOT NULL,
    `crateComplaint` VARCHAR(255) NULL,
    `status` VARCHAR(255) default 'PENDING', 
    `isClosed` BOOLEAN  default 0, 
    `isVerified` BOOLEAN  default 0,
    `iaErrors` VARCHAR(255) ,
    `comments` VARCHAR(255) NULL,
    `finalAmount` FLOAT(11)  default 0,
    `incentiveAmount` FLOAT(11)  default 0,
    `suUnitId` INT(11) NOT NULL,
    `isDispatched` BOOLEAN  DEFAULT 0,
    `isReturnVerified` BOOLEAN  default 0,
    `uniqueSaleOrderId` VARCHAR(255) NULL,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_sales_unit_orders`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_sales_unit_orders` FOREIGN KEY (`suUserId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk2_su_sales_unit_orders` FOREIGN KEY (`suUnitId`) REFERENCES `cd_sales_unit` (`id`);

CREATE TABLE `su_sales_unit_order_items` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `suOrderId` INT(11) NOT NULL,
    `productId` INT(11) NOT NULL,
    `orderedQuantity` INT(11) NOT NULL,
    `price` float(20)  default 0.0 ,
    `gst` float(10),
    `receivedQuantity` INT(11)  default 0,
    `returnedQuantity` INT(11)  default 0,
    `iuitDeductedQuantity` INT(11)  default 0,
    `iuitAddedQuantity` INT(11)  default 0,
    `subTotal` float(20)  default 0,
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_sales_unit_order_items`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_sales_unit_order_items` FOREIGN KEY (`suOrderId`) REFERENCES `su_sales_unit_orders` (`id`),
    ADD CONSTRAINT `fk2_su_sales_unit_order_items` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`);

CREATE TABLE `su_sales_unit_payment` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `suOrderId` INT(11) NOT NULL,
    `paymentDetailsId` INT(11) NOT NULL,
    `status` VARCHAR(255) default 'PENDING',
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_sales_unit_payment`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_sales_unit_payment` FOREIGN KEY (`suOrderId`) REFERENCES `su_sales_unit_orders` (`id`),
    ADD CONSTRAINT `fk2_su_sales_unit_payment` FOREIGN KEY (`paymentDetailsId`) REFERENCES `cd_payment_details` (`id`);

CREATE TABLE `su_iuit_orders` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `suOrderId` INT(11) NOT NULL,
    `requesterUserId` INT(11) NOT NULL,
    `senderUserId` INT(11) NOT NULL,
    `senderApproval` BOOLEAN  default '0',
    `createdDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_iuit_orders`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_iuit_orders` FOREIGN KEY (`suOrderId`) REFERENCES `su_sales_unit_orders` (`id`),
    ADD CONSTRAINT `fk2_su_iuit_orders` FOREIGN KEY (`requesterUserId`) REFERENCES `cd_users` (`id`),
    ADD CONSTRAINT `fk3_su_iuit_orders` FOREIGN KEY (`senderUserId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `su_iuit_order_items` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `iuitId` INT(11) NOT NULL,
    `productId` INT(11) NOT NULL,
    `quantity` INT(11),
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_iuit_order_items`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_iuit_order_items` FOREIGN KEY (`iuitId`) REFERENCES `su_iuit_orders` (`id`),
    ADD CONSTRAINT `fk2_su_iuit_order_items` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`);

CREATE TABLE `su_sales_unit_price`(
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11) NOT NULL,
    `date` TIMESTAMP NOT NULL,
    `price` float(20) NOT NULL,
    `salesUnitId` INT(11) NOT NULL, 
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_sales_unit_price`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_sales_unit_price` FOREIGN KEY (`salesUnitId`) REFERENCES `cd_sales_unit` (`id`),
    ADD CONSTRAINT `fk2_su_sales_unit_price` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`);

CREATE TABLE `su_sales_unit_price_history`(
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `productId` INT(11) NOT NULL,
    `date` TIMESTAMP NOT NULL,
    `price` float(20) NOT NULL,
    `salesUnitId` INT(11) NOT NULL,
    `userId` INT(11),
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `su_sales_unit_price_history`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_su_sales_unit_price_history` FOREIGN KEY (`salesUnitId`) REFERENCES `cd_sales_unit` (`id`),
    ADD CONSTRAINT `fk2_su_sales_unit_price_history` FOREIGN KEY (`productId`) REFERENCES `cd_products` (`id`),
    ADD CONSTRAINT `fk3_su_sales_unit_price_history` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);






CREATE TABLE `quality_check` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `purchaseOrderId` INT(11) NOT NULL,
    `vendorProductId` INT(10) NOT NULL,
    `totalQuantity` FLOAT(25),
    `itemTotalPieces` INT(11),
    `gradeA` FLOAT(25) NULL,
    `manualGradeA` FLOAT(25) NULL,
    `companyName` VARCHAR(255) NULL,
    `billDate` DATETIME NULL,
    `billNumber` VARCHAR(255) NULL,
    `weighBillReceiptId` INT(11) NULL,
    `totalVehicleWeight` float(25) NULL,
    `emptyVehicleWeight` float(25) NULL,
    `isQcCompleted` BOOLEAN  default 1,
    PRIMARY KEY (`id`)
);

ALTER TABLE `quality_check`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_quality_check` FOREIGN KEY (`purchaseOrderId`) REFERENCES `vms_vendor_purchase_order` (`id`),
    ADD CONSTRAINT `fk2_quality_check` FOREIGN KEY (`vendorProductId`) REFERENCES `vms_vendor_purchase_order_item` (`vendorProductId`),
    ADD CONSTRAINT `fk3_quality_check` FOREIGN KEY (`weighBillReceiptId`) REFERENCES `cd_file` (`id`);

CREATE TABLE `cd_crates` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `qrCode` VARCHAR(255) NULL,
    `isAvailable` boolean  default '1',
    `price` INT(11),
    `isPaid` boolean  default '0',
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);
ALTER TABLE `cd_crates`
    AUTO_INCREMENT = 101;

CREATE TABLE `cd_assigned_crates` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `crateId` INT(11) NOT NULL,
    `orderId` INT(11) NOT NULL,
    `isReturned` BOOLEAN  default 0,
    `isReceived` BOOLEAN  default 0,
    `isUnReturned` BOOLEAN  default 0,
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`)
);

ALTER TABLE `cd_assigned_crates`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_assigned_crates` FOREIGN KEY (`crateId`) REFERENCES `cd_crates` (`id`),
    ADD CONSTRAINT `fk2_cd_assigned_crates` FOREIGN KEY (`orderId`) REFERENCES `su_sales_unit_orders` (`id`);

CREATE TABLE `customer_refund` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `ordersId` INT(10) NOT NULL,
    `paymentDetailsId` INT(11),
    `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

ALTER TABLE `customer_refund`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_customer_refund` FOREIGN KEY (`ordersId`) REFERENCES `cd_orders` (`id`),
    ADD CONSTRAINT `fk2_customer_refund` FOREIGN KEY (`paymentDetailsId`) REFERENCES `cd_payment_details` (`id`);


CREATE TABLE `cd_vehicle_assignment` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `departureDate` DATE NOT NULL,
    `departureTime` TIME,
    `vehicleId` INT(11) NOT NULL,
    `driverId` INT(255) NOT NULL,
    `userId` INT(11) NOT NULL,
    `startDateTime` TIMESTAMP NULL,
    `endDateTime` TIMESTAMP NULL,
    `uniqueVehicleAssignmentId` VARCHAR(255) NULL,		
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_vehicle_assignment`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_vehicle_assignment` FOREIGN KEY (`vehicleId`) REFERENCES `lg_vehicle_details` (`id`),
    ADD CONSTRAINT `fk2_cd_vehicle_assignment` FOREIGN KEY (`driverId`) REFERENCES `lg_driver_details` (`id`),
    ADD CONSTRAINT `fk3_cd_vehicle_assignment` FOREIGN KEY (`userId`) REFERENCES `cd_users` (`id`);

CREATE TABLE `cd_vehicle_assignment_orders` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `sopId` INT(11) NULL,
    `poId` INT(11) NULL,
    `orderId` INT(11) NULL,
    `vehicleAssignedId` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `cd_vehicle_assignment_orders`
    AUTO_INCREMENT = 101,
    ADD CONSTRAINT `fk1_cd_vehicle_assignment_orders` FOREIGN KEY (`sopId`) REFERENCES `su_sales_unit_orders` (`id`),
    ADD CONSTRAINT `fk2_cd_vehicle_assignment_orders` FOREIGN KEY (`poId`) REFERENCES `vms_vendor_purchase_order` (`id`),
    ADD CONSTRAINT `fk3_cd_vehicle_assignment_orders` FOREIGN KEY (`orderId`) REFERENCES `cd_orders` (`id`),
    ADD CONSTRAINT `fk4_cd_vehicle_assignment_orders` FOREIGN KEY (`vehicleAssignedId`) REFERENCES `cd_vehicle_assignment` (`id`);
INSERT INTO `ariantveg`.`cd_auth_client` (`name`, `clientId`, `clientSecret`, `redirectUri`) VALUES ('ariant', '1000', 'ariantvegtech', 'https://portal.ariantveg.com');

INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('ADMIN', 'ADMIN');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('VMS', 'Vendor Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('BD', 'Business Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('EXPENSES', 'Expense Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('LOGISTICS', 'Logistics Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('DRIVER', 'Driver Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('SECURITY', 'Security Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('PRODUCTION', 'Production Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('SALES', 'Sales Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('INTERNAL_AUDIT', 'Internal Audit Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('ACCOUNTS', 'Account Manager');
INSERT INTO `ariantveg`.`cd_role` (`roleType`, `description`) VALUES ('CUSTOMER', 'Customer');

INSERT INTO `ariantveg`.`cd_config_type` (`name`, `typeEnum`, `description`) VALUES ('PETTYCASH', 'PETTYCASH', 'PETTYCASH');
INSERT INTO `ariantveg`.`cd_config` (`configTypeId`, `amount`) VALUES ((SELECT id FROM cd_config_type WHERE typeEnum='PETTYCASH'), '0');

INSERT INTO `ariantveg`.`cd_password` (`id`, `passwordHash`) VALUES ('101', '$2a$10$pNIx8m4W1d7b9sRcTsa/h.njFBy76FDoydKRvd7BVdiWNkNqRWuGy');
INSERT INTO `ariantveg`.`cd_users` (`name`, `email`, `phone`, `passwordId`, `uniqueUserId`) VALUES ('Yogesh', 'info@ariant.in', '9844466570', '101','AU19FEB101');
INSERT INTO `ariantveg`.`cd_users_role` (`userId`, `roleId`) VALUES ('101', '101');

INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('BABY CORN', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('FRESH BUTTON MUSHROOM', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('FRESH OYSTER MUSHROOM(100 GM)', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('FRESH OYSTER MUSHROOM(200 GM)', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('PORTABELLO MUSHROOM(200 GRAM)', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('SPROUTS', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('SWEET CORN', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('BABT POTATO', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('CAPSICUM', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('WHITE ONION', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('FRESH GREEN PEAS', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('GARLIC', 0);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('FROZEN GREEN PEAS', 5);
INSERT INTO `ariantveg`.`vms_purchase_products` (`productName`, `gstPercentage`) VALUES ('FRESH SWEET CORN', 5);

